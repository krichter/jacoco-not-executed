package de.richtercloud.jacoco.not.executed;

import static org.junit.jupiter.api.Assertions.*;

class SomeClassTest {

    @org.junit.jupiter.api.Test
    void someMethod() {
        SomeClass instance = new SomeClass();
        String result = instance.someMethod("abc");
        assertEquals("bc", result);
    }
}